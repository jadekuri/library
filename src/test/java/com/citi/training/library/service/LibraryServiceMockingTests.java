package com.citi.training.library.service;

import static org.mockito.Mockito.when;

import com.citi.training.library.dao.LibraryMongoRepo;
import com.citi.training.library.model.LibraryItem;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;


@SpringBootTest
public class LibraryServiceMockingTests {

    @Autowired
    private LibraryService libraryService;

    @MockBean
    private LibraryMongoRepo mockLibraryRepo;

    @Test
    public void test_libraryService_save() {

        LibraryItem testItem = new LibraryItem();
        testItem.setType("DVD");
        testItem.setTitle("Test Title");
        testItem.setBorrowed(true);

        when(mockLibraryRepo.save(testItem)).thenReturn(testItem);
        libraryService.save(testItem);
    }

    
}
