package com.citi.training.library.service;

import com.citi.training.library.model.LibraryItem;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LibraryServiceTests {

    private static Logger LOG = LoggerFactory.getLogger(LibraryServiceTests.class);

    @Autowired
    private LibraryService libraryService;

    @Test
    public void test_Save() {
        LOG.info("Test log message - save");

        LibraryItem item1 = new LibraryItem();
        item1.setType("Book");
        item1.setTitle("Book Title");
        item1.setBorrowed(true);
        libraryService.save(item1);
    }

    @Test
    public void test_FindAll() {
        LOG.info("Test log message - findAll");
        assert(libraryService.findAll().size() > 0);
    }

    @Test
    public void test_DeleteItem() {
        LibraryItem item3 = new LibraryItem();
        item3.setType("Periodical");
        item3.setTitle("Periodical Title");
        item3.setBorrowed(false);
        libraryService.save(item3);        
        libraryService.delete(item3);
    }

    @Test
    public void test_Borrow() {
        LibraryItem item4 = new LibraryItem();
        item4.setType("DVD");
        item4.setTitle("DVD Title");
        item4.setBorrowed(true);
        libraryService.save(item4);

        libraryService.borrow(item4, false);
        assert(item4.isBorrowed() == false);
    }
        
}
