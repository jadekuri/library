package com.citi.training.library.dao;

import com.citi.training.library.model.LibraryItem;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface LibraryMongoRepo extends MongoRepository<LibraryItem, String> {
    
}
