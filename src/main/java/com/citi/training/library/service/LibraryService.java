package com.citi.training.library.service;

import java.util.List;

import com.citi.training.library.dao.LibraryMongoRepo;
import com.citi.training.library.model.LibraryItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This is the main business logic class for Library
 * @see LibraryItem
 */

@Component
public class LibraryService {
    
    @Autowired
    private LibraryMongoRepo mongoRepo;

    public List<LibraryItem> findAll() {
        return mongoRepo.findAll();
    }

    public LibraryItem save(LibraryItem libraryItem) {
        return mongoRepo.save(libraryItem);
    }

    public void delete(LibraryItem libraryItem) {
        mongoRepo.delete(libraryItem);
    }

    public void borrow(LibraryItem libraryItem, boolean borrowed) {
        libraryItem.setBorrowed(borrowed);    
    }

}
